const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {path: path.resolve(__dirname, 'dist'), filename: 'bundle.js'},
  module: {
    rules: [{
      test: /\.(png|svg|jpg|gif|json)$/,
      use: [
        'file-loader',
      ],
    }]
  }
};