
import * as PIXI from 'pixi.js'
import {Z_ASCII} from 'zlib'

import hitTestRectangle from './hittestrectangle'
import Log from './img/log.png'
import OverworldRectangles from './img/Overworld'
import Overworld from './img/Overworld.png'
import keyboard from './keyboard'
import levels from './levels'

const textureCache = new Map();

const sprite =
    function(img, rects, name) {
  if (textureCache.has(name)) {
    return new PIXI.Sprite(textureCache.get(name));
  } else {
    const tx = new PIXI.Texture(loader.resources[Overworld].texture);
    const rectMap = OverworldRectangles[name];
    tx.frame = new PIXI.Rectangle(rectMap.x, rectMap.y, rectMap.w, rectMap.h);

    textureCache.set(name, tx);
    return new PIXI.Sprite(tx)
  }
}

function createLevel(number) {
  const levelGroup = new PIXI.Container();
  const m = levels.mapping;
  const level = levels.levels[number];
  const cols = 31;
  const len = level.length;

  const begX = 12;
  const begY = 12;

  for (var i = 0; i < len; ++i) {
    const s = sprite(Overworld, OverworldRectangles, 'grass');
    s.zIndex = -10;
    s.x = begX + i % cols * 25;
    s.y = begY + ((i / cols) | 0) * 25;
    levelGroup.addChild(s);
  }

  const result = {
    player: null,
    deads: [],
    stops: [],
    level: levelGroup,
    idx: number,
    finish: null
  };

  for (var i = 0; i < len; ++i) {
    const name = m[level.charAt(i)];
    if (name == 'grass') continue;

    const s = sprite(Overworld, OverworldRectangles, name);
    s.x = begX + i % cols * 25;
    s.y = begY + ((i / cols) | 0) * 25;

    if (name == 'player') {
      result.player = s;
      s.vx = 0;
      s.vy = 0;
      s.zIndex = 100;
    }
    if (name == 'hole') result.deads.push(s);
    if (name == 'rock') result.stops.push(s);
    if (name == 'finish') result.finish = s;

    levelGroup.addChild(s);
  }

  return result;
}

function gameLoop(level, delta) {
  const player = level.player;
  let playerHitTheStop = false;
  for (var i = 0, len = level.stops.length; i < len && !playerHitTheStop; ++i) {
    const stop = level.stops[i];
    if (hitTestRectangle(player, stop)) {
      player.vx *= -1;
      player.vy *= -1;

      playerHitTheStop = true;
    }
  }

  for (var i = 0, len = level.deads.length; i < len && !playerHitTheStop; ++i) {
    const d = level.deads[i];
    if (hitTestRectangle(player, d)) {
      dead();
    }
  }

  if (hitTestRectangle(player, level.finish)) {
    nextLevel();
  }

  if (player.x < 12 || player.y < 12 || player.x > 800 - 12 ||
      player.y > 600 - 12)
    dead();

  player.x += player.vx;
  player.y += player.vy;

  if (playerHitTheStop) {
    player.vx = 0;
    player.vy = 0;
  }
}


function
setupKeyboard() {
  // Capture the keyboard arrow keys
  let left = keyboard('ArrowLeft'), up = keyboard('ArrowUp'),
      right = keyboard('ArrowRight'), down = keyboard('ArrowDown');

  // Left arrow key `press` method
  left.press = () => {
    // Change the player's velocity when the key is pressed
    currentLevel.player.vx = -5;
    currentLevel.player.vy = 0;
  };

  // Left arrow key `release` method
  /*left.release = () => {
    // If the left arrow has been released, and the right arrow isn't down,
    // and the player isn't moving vertically:
    // Stop the player
    if (!right.isDown && player.vy === 0) {
      player.vx = 0;
    }
  };*/

  // Up
  up.press = () => {
    currentLevel.player.vy = -5;
    currentLevel.player.vx = 0;
  };
  /*up.release = () => {
    if (!down.isDown && player.vx === 0) {
      player.vy = 0;
    }
  };*/

  // Right
  right.press = () => {
    currentLevel.player.vx = 5;
    currentLevel.player.vy = 0;
  };
  /*right.release = () => {
    if (!left.isDown && player.vy === 0) {
      player.vx = 0;
    }
  };*/

  // Down
  down.press = () => {
    currentLevel.player.vy = 5;
    currentLevel.player.vx = 0;
  };
  /*down.release = () => {
    if (!up.isDown && player.vx === 0) {
      player.vy = 0;
    }
  };*/
}

function
dead() {
  app.stage.removeChild(currentLevel.level);

  currentLevel = createLevel(0);
  app.stage.addChild(currentLevel.level);
}

function
nextLevel() {
  app.stage.removeChild(currentLevel.level);

  currentLevel = createLevel((currentLevel.idx + 1) % levels.levels.length);
  app.stage.addChild(currentLevel.level);
}

var currentLevel = null;

const app = new PIXI.Application();
document.body.appendChild(app.view);

const loader = new PIXI.Loader('./dist/');
loader.add([Overworld, Log]).load(() => {
  currentLevel = createLevel(0);
  setupKeyboard();
  app.stage.addChild(currentLevel.level);

  app.ticker.add(delta => gameLoop(currentLevel, delta));

  app.start();
});
