export default {
  grass: {
    x: 115,
    y: 146,
    w: 141 - 116,
    h: 175 - 147,
  },
  hole: {x: 186, y: 403, w: 32, h: 32},
  rock: {x: 234, y: 282, w: 32, h: 32},
  player: {x: 483, y: 325, w: 14, h: 21},
  finish: {x: 417, y: 320, w: 14, h: 21}
};